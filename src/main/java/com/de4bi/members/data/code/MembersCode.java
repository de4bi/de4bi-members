package com.de4bi.members.data.code;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public enum MembersCode {

    // Authority
    MEMBERS_AUTHORITY_BASIC     (1, "MEMBERS_AUTHORITY","AU00","준회원"),
    MEMBERS_AUTHORITY_STANDARD  (2, "MEMBERS_AUTHORITY","AU01","정회원"),
    MEMBERS_AUTHORITY_PREMIUM   (3, "MEMBERS_AUTHORITY","AU02","프리미엄"),
    MEMBERS_AUTHORITY_MANAGER   (4, "MEMBERS_AUTHORITY","AU98","운영진"),
    MEMBERS_AUTHORITY_ADMIN     (5, "MEMBERS_AUTHORITY","AU99","관리자"),

    // Status
    MEMBERS_STATUS_NORMAL       (101, "MEMBERS_STATUS","ST00","일반"),
    MEMBERS_STATUS_SLEEP        (102, "MEMBERS_STATUS","ST01","휴면"),
    MEMBERS_STATUS_BANNED       (103, "MEMBERS_STATUS","ST02","정지"),
    MEMBERS_STATUS_DEREGISTER   (104, "MEMBERS_STATUS","ST03","탈퇴");

    private final long   seq;
    private final String groups;
    private final String value;
    private final String name;
}